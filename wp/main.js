document.onreadystatechange = function() { 
	if (document.readyState !== "complete") { 
		document.querySelector("body").style.visibility = "hidden"; 
        document.querySelector("#hide-menu").style.visibility = "visible"; 
        $('#menu').animate({opacity: 0}, 200);
        $('#menu').animate({opacity: 1}, 500);
		document.querySelector("#hide-menu").style.visibility = "visible"; 
        $('#inside-menu').animate({opacity: 0}, 500);
        $('#inside-menu').animate({opacity: 1}, 500);
	} else { 
		document.querySelector("body").style.visibility = "visible"; 
	} 
}; 


var openPopUp = function() {
    document.getElementById("popup1window").style.display="inline";
}

var closePopUp = function() {
    document.getElementById("popup1window").style.display="none";
}

var tour=pannellum.viewer('panorama', {   
    "default": {
        "firstScene": "main",
        "sceneFadeDuration": 1000,
        "autoLoad": true
    },

    "scenes": {
        "main": {
            "title": "View One",
            "type": "equirectangular",
            "panorama": "images/360.jpg",
            "hotSpots": [
                {
                    "pitch": -25,
                    "yaw": 185,
                    "type": "info",
                    "text": "Pop Up",
                    "cssClass": "popup-click",
                    "id": "popup1",
                    "clickHandlerFunc": openPopUp
                },
                {
                    "pitch": -13,
                    "yaw": 156,
                    "type": "scene",
                    "text": "Scene 2",
                    "sceneId": "sideview",
                    "id": "tour1"
                }
            ]
        },

        "sideview": {
            "title": "Side View",
            "hfov": 160,
            "yaw": 5,
            "type": "equirectangular",
            "panorama": "images/360_2.png",
            "hotSpots": [
                {
                    "pitch": -5,
                    "yaw": 0,
                    "type": "scene",
                    "text": "View Main",
                    "sceneId": "main",
                    "targetYaw": -23,
                    "targetPitch": -12
                }
            ]
        }
    }
});

$("#hide-button" ).click(function() {
    $( "#hide-menu" ).slideUp("slow");
});   

$('.button-int').click(function() {
    var id=$(this).attr('id');
    if($(this).hasClass(id+"-hidden")){
        hideAll();
    }
    $(this).toggleClass(id+"-hidden");
    $("#"+id+"-toggle").toggleClass('flip');    
});

function hideAll(){
    $(".button-int").each(function(index, item){
        var el=$(item).attr('id');
        $(item).addClass(el+"-hidden");
        $("#"+el+"-toggle").removeClass("flip"); 
    });
}

var switchplay = true;
var audioElement = document.getElementById('audioplayer');
function changeMusic(){
    if(switchplay){
        audioElement.play();
        $("#icon1").hide();
        $("#icon2").show();
    }
    else{
        audioElement.pause();  
        $("#icon2").hide();
        $("#icon1").show();          
    }
    switchplay=!switchplay; 
}