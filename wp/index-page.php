<?php /* Template Name: IndexPage */ ?>

<script type="text/javascript">
    var templateUrl = '<?= get_bloginfo("template_url"); ?>';
</script>

<style>
body,html{height:100%;width:100%;margin:0;overflow:hidden}
#masthead{
    visibility:hidden;
    display:none;
}
.footer-width-fixer{
	position:absolute !important;
	bottom:0;
}

#panorama{
    display:none;
}

.music-absolute{
    position:fixed;
    z-index:80;
    bottom:100px;
    padding:10px 10px;
    display:block;    
    border-radius: 50%;
    outline:0;
    border:none;
    cursor:pointer;
    transition:0.3s;
    background-color: rgb(247 245 265 / 30%);
    box-shadow: 2px 2px 6px rgb(40 40 40 / 60%);
}

.medium-btn{
    height: 50px;
    width: 50px;
}

.big-btn{
    height: 70px;
    width: 70px;
}
  
.music-absolute .play-button{
    width:26px;
    margin-top:2px;
    margin-left:-2px;
}

.music-absolute .stop-button{
    width:20px;
    margin-top:3px;
    margin-left:1px;
}

.wolf-loading{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
    text-align: center;
    width: 35%;
    z-index:100000;
}

#load-menu{
    display:none;
    width:100%;
    height:100%;
    position:absolute;
    z-index:100000;
    background-color: rgba(0,0,0,0.6);
}

#page{
    width:100%;
    height:100%;
}

.overlay-popup{
    width:100%;
    height:100%;
    background-color: rgba(0,0,0,0.75);
    z-index:1000000;
    display:none;
    position:absolute;
}

.popup{
    position:absolute;
    top:50%;
    left:50%;
    transform:translate(-50%,-50%);
    width:80%;
    height:80%;
    z-index:10;    
    background-color:rgba(40,40,40,.8);
    color:#fff;
    text-align:center
}

.adjust{
    bottom:15px;
}

.left{
    left:30px;
}

.right{
    right:30px;
}

.far-right{
    right:110px;
}

.icon-sm{
    width:34px;
}

@media only screen and (max-width: 768px) {
  .music-absolute{
      bottom:15px !important;
  }
  .wolf-gif{
      width:420px;
  }  
  .menu-indent{
      width:340px;
  }
  .menu-indent h1{
      font-size: 48px;
  }
  .menu-indent p{
      font-size:26px;
  }
  .wolf-loading{
    width: 70% !important;
  }
  .popup-click{
    width:30px !important;
    height:30px !important;
  }
}

@media only screen and (max-width: 460px) {
  .wolf-gif{
      width:340px;
  }
  .menu-indent{
      width:300px;
  }
  .menu-indent p{
      font-size:22px;
  }
  .menu-indent h1{
      font-size: 42px;
  }  
  body,html{overflow-y:scroll}
}
</style>

<?php get_header(); ?>

<div class="container" id="hide-menu">
    <div id="menu" class="menu">
        <div class="content">
            <img class="wolf-gif" src="<?php echo get_template_directory_uri(); ?>/assets/resources/logo-idartes.gif">
            <div class="menu-indent null-opacity architect-bold" id="inside-menu">
                <h1>NENCATACOA</h1>
                <div class="red-bar"></div>
                <p>Plataforma artística de los pueblos étnicamente diferenciales residentes en Bogotá</p>
                <div class="full-width">
                    <button id="hide-button">Ingresa a la experiencia</button>
                </div>     
                <img class="logos" src="<?php echo get_template_directory_uri(); ?>/assets/resources/marcas-idartes.png" />   
            </div>
        </div>        
    </div>
</div>
<button class="btn play-button music-absolute left big-btn" onclick="changeMusic()">
    <img class="play-button" src="https://nencatacoa.co/wp-content/uploads/2021/04/music-on.png" id="icon1" class="icon-sm play-icon">
    <img  class="stop-button" src="https://nencatacoa.co/wp-content/uploads/2021/04/music-off.png" id="icon2" class="icon-sm play-icon">
</button>

<div id="load-menu">
    <img class="wolf-loading" src="<?php echo get_template_directory_uri(); ?>/assets/resources/logo-idartes1-loop_blanco.gif">
</div>

<div id="panorama">
    <div id="panorama-controls" class="hide">        
        <button class="btn music-absolute right medium-btn" id="full-button">
            <img class="icon-sm" src="<?php echo get_template_directory_uri(); ?>/assets/resources/full-screen.png">
        </button>
        <button class="btn music-absolute far-right medium-btn" id="button-open-menu">
            <img class="icon-sm" src="<?php echo get_template_directory_uri(); ?>/assets/resources/mapa.png">
        </button>
    </div>
</div>
<div class="overlay-popup" id="overlay">
    <div id="popup1window" class="popup">
        <div class="popupcontainer">
            <button class="absolute-button" onclick="closePopUp()">X</button>
            <div id="frame-reloader">
                <iframe id="popup-frame" src="/home-experience/popup1" frameBorder="0" style="width: 100%; height: 100%;"></iframe>
            </div>
        </div>
    </div>
</div>
<audio id="audioplayer" src="https://nencatacoa.co/wp-content/uploads/2021/04/recorrido.mp3" type="audio/mp3"></audio>

<?php echo do_shortcode("[hfe_template id='2158']"); ?>

<script type="text/javascript">

document.onreadystatechange = function() { 
	if (document.readyState !== "complete") { 
		document.querySelector("body").style.visibility = "hidden"; 
        document.querySelector("#hide-menu").style.visibility = "visible"; 
        $('#menu').animate({opacity: 0}, 200);
        $('#menu').animate({opacity: 1}, 500);
		document.querySelector("#hide-menu").style.visibility = "visible"; 
        $('#inside-menu').animate({opacity: 0}, 500);
        $('#inside-menu').animate({opacity: 1}, 500);
	} else { 
		document.querySelector("body").style.visibility = "visible"; 
	} 
}; 

var device="desktop";
var default_fov=120;
var default_scene="scene_5";

const queryString = window.location.search;

const urlParams = new URLSearchParams(queryString);


if ($(window).width() < 768){
    device="mobile";
    default_fov=70;
}

var openPopUp = function(args, popup) {
    $("#frame-reloader").empty();
    $("#frame-reloader").append('<iframe id="popup-frame" frameBorder="0" style="width: 100%; height: 100%;" src="' + popup + '" />');
    $("#overlay").show();    
}

var closePopUp = function() {
    $("#overlay").hide();
}

var sceneList={
    "scene_1": {
        "title": "Hytcha Guy",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/1.jpg',
        "hfov": default_fov,
        "pitch": 7,
        "yaw": -179,
        "hotSpots": [                
            {
                "pitch": 4,
                "yaw": 2,
                "type": "scene",
                "text": "Pabellón A-2",
                "sceneId": "scene_17",
                "id": "scene-17"
            },
            {
                "pitch": 7,
                "yaw": -179,
                "type": "info",
                "text": "Hytcha Guy",
                "cssClass": "popup-click",
                "id": "popup1",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-hytcha-guy"
            }
        ]
    },
    "scene_2": {
        "title": "Becas General",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/2.jpg',
        "pitch": -4,
        "yaw": 176,
        "hfov": default_fov,
        "hotSpots": [                
            {
                "pitch": 6,
                "yaw": 1,
                "type": "scene",
                "text": "Pabellón A-1",
                "sceneId": "scene_10",
                "id": "scene-10"
            },
            {
                "pitch": -4,
                "yaw": 176,
                "type": "scene",
                "text": "Becas",
                "sceneId": "scene_8",
                "id": "scene-8"
            }
        ]
    },
    "scene_3": {
        "title": "Opre Rroma",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/3.jpg',
        "pitch": 2,
        "yaw": 177,
        "hfov": default_fov,
        "hotSpots": [                
            {
                "pitch": 10,
                "yaw": -7,
                "type": "scene",
                "text": "Pabellón A-3",
                "sceneId": "scene_14",
                "id": "scene-14"
            },
            {
                "pitch": 2,
                "yaw": 177,
                "type": "info",
                "text": "Opre Rroma",
                "cssClass": "popup-click",
                "id": "popup2",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-opre-rroma"
            }
        ]
    },
    "scene_4": {
        "title": "Arte para la interculturalidad",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/4.jpg',
        "hfov": default_fov,
        "pitch": 8,
        "yaw": -1,
        "hotSpots": [                
            {
                "pitch": 2,
                "yaw": -177,
                "type": "scene",
                "text": "Pabellón A-4",
                "sceneId": "scene_16",
                "id": "scene-16"
            },
            {
                "pitch": 8,
                "yaw": -1,
                "type": "info",
                "text": "Artes interculturales",
                "cssClass": "popup-click",
                "id": "popup3",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-arte-para-la-interculturalidad"
            }
        ]
    },
    "scene_5": {
        "title": "Espacio General",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/5.jpg',
        "hfov": default_fov,
        "pitch": -2,
        "yaw": 6,
        "hotSpots": [                
            {
                "pitch": -2,
                "yaw": -151,
                "type": "scene",
                "text": "Pabellón B",
                "sceneId": "scene_6",
                "id": "scene-6"
            },
            {
                "pitch": -4,
                "yaw": 56,
                "type": "scene",
                "text": "Pabellón A-1",
                "sceneId": "scene_10",
                "id": "scene-10"
            },
            {
                "pitch": -2,
                "yaw": -42,
                "type": "scene",
                "text": "Pabellón A-4",
                "sceneId": "scene_16",
                "id": "scene-16"
            }
        ]
    },
    "scene_6": {
        "title": "Pabellón B",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/6.jpg',
        "hfov": default_fov,
        "pitch": 1,
        "yaw": 8,
        "hotSpots": [                
            {
                "pitch": -2,
                "yaw": -6,
                "type": "scene",
                "text": "Espacio General",                    
                "sceneId": "scene_5",
                "id": "scene-5"
            },
            {
                "pitch": -3,
                "yaw": -121,
                "type": "scene",
                "text": "Fechas Conmemorativas",                    
                "sceneId": "scene_11",
                "id": "scene-11"
            },
            {
                "pitch": -1,
                "yaw": 66,
                "type": "scene",
                "text": "Eventos no Idartes con representatividad de impacto en la ciudad",                    
                "sceneId": "scene_13",
                "id": "scene-13"
            },
            {
                "pitch": -1,
                "yaw": 123,
                "type": "scene",
                "text": "Calendario artístico de los grupos étnicos",                    
                "sceneId": "scene_12",
                "id": "scene-12"
            }
        ]
    },
    "scene_7": {
        "title": "Por los senderos del arte indígena en Bogotá",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/7.jpg',
        "hfov": default_fov,
        "pitch": 9,
        "yaw": -180,
        "hotSpots": [                
            {
                "pitch": 6,
                "yaw": -2,
                "type": "scene",
                "text": "Pabellón A-4",
                "sceneId": "scene_16",
                "id": "scene-16"
            },
            {
                "pitch": 9,
                "yaw": -180,
                "type": "info",
                "text": "Por los senderos del arte indígena en Bogotá",
                "cssClass": "popup-click",
                "id": "popup6",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-por-los-senderos-del-arte"
            }
        ]
    },
    "scene_8": {
        "title": "Becas",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/8.jpg',
        "hfov": default_fov,
        "pitch": 4,
        "yaw": -7,
        "hotSpots": [                
            {
                "pitch": 6,
                "yaw": 178,
                "type": "scene",
                "text": "Pabellón A-1",
                "sceneId": "scene_10",
                "id": "scene-10"
            },
            {
                "pitch": -5,
                "yaw": -67,
                "type": "info",
                "text": "Becas 2017",
                "cssClass": "popup-click",
                "id": "popup7",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-becas-2017"
            },
            {
                "pitch": -4,
                "yaw": -43,
                "type": "info",
                "text": "Becas 2018",
                "cssClass": "popup-click",
                "id": "popup8",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-becas-2018"
            },
            {
                "pitch": -4,
                "yaw": -6,
                "type": "info",
                "text": "Becas 2019",
                "cssClass": "popup-click",
                "id": "popup9",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-becas-2019"
            },
            {
                "pitch": -6,
                "yaw": 32,
                "type": "info",
                "text": "Becas 2020",
                "cssClass": "popup-click",
                "id": "popup19",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-becas-2020"
            },
            {
                "pitch": -10,
                "yaw": 56,
                "type": "info",
                "text": "Becas activación web",
                "cssClass": "popup-click",
                "id": "popup20",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-becas-activacion-web"
            }
        ]
    },
    "scene_9": {
        "title": "Muestra Afro",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/9.jpg',
        "hfov": default_fov,
        "pitch": 11,
        "yaw": -174,
        "hotSpots": [                
            {
                "pitch": 6,
                "yaw": -5,
                "type": "scene",
                "text": "Pabellón A-3",
                "sceneId": "scene_14",
                "id": "scene-14"
            },
            {
                "pitch": 11,
                "yaw": -174,
                "type": "info",
                "text": "Muestra Afro",
                "cssClass": "popup-click",
                "id": "popup10",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-muestra-afro"
            }
        ]
    },
    "scene_10": {
        "title": "Pabellón A-1",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/10.jpg',
        "hfov": default_fov,
        "hotSpots": [                
            {
                "pitch": -1,
                "yaw": -161,
                "type": "scene",
                "text": "Manuel Zapata Olivella",
                "sceneId": "scene_21",
                "id": "scene-21"
            },
            {
                "pitch": -2,
                "yaw": -108,
                "type": "scene",
                "text": "Espacio General",
                "sceneId": "scene_5",
                "id": "scene-5"
            },
            {
                "pitch": 0,
                "yaw": 34,
                "type": "scene",
                "text": "Pabellón A-2",
                "sceneId": "scene_17",
                "id": "scene-17"
            },
            {
                "pitch": -1,
                "yaw": 100,
                "type": "scene",
                "text": "Becas Idartes",
                "sceneId": "scene_2",
                "id": "scene-2"
            },
            {
                "pitch": 0,
                "yaw": 73,
                "type": "scene",
                "text": "Exposición Embera",
                "sceneId": "scene_19",
                "id": "scene-19"
            },
            {
                "pitch": -4,
                "yaw": 139,
                "type": "scene",
                "text": "Mesas de arte",
                "sceneId": "scene_20",
                "id": "scene-20"
            }
        ]
    },
    "scene_11": {
        "title": "Sala de fechas conmemorativas",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/11.jpg',
        "hfov": default_fov,
        "pitch": 7,
        "yaw": -43,
        "hotSpots": [                
            {
                "pitch": 1,
                "yaw": 162,
                "type": "scene",
                "text": "Pabellón B",
                "sceneId": "scene_6",
                "id": "scene-6"
            }
        ]
    },
    "scene_12": {
        "title": "Calendario artístico de los grupos étnicos",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/12.jpg',
        "hfov": default_fov,
        "pitch": 6,
        "yaw": 1,
        "hotSpots": [                
            {
                "pitch": -2,
                "yaw": 177,
                "type": "scene",
                "text": "Pabellón B",
                "sceneId": "scene_6",
                "id": "scene-6"
            },
            {
                "pitch": 0,
                "yaw": 20,
                "type": "info",
                "text": "Calendario artístico de los grupos étnicos",
                "cssClass": "popup-click",
                "id": "popup11",
                "type": "info",
                "URL": "https://www.google.com/maps/d/u/0/viewer?ll=3.270182922103367%2C-76.394944&z=8&mid=1WS977tRkiIovsq_Wo48Am6TSQqQm55zH"
            }
        ]
    },
    "scene_13": {
        "title": "Eventos no Idartes con representatividad de impacto en la ciudad",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/13.jpg',
        "hfov": default_fov,
        "pitch": 3,
        "yaw": 34,
        "hotSpots": [                
            {
                "pitch": -4,
                "yaw": -174,
                "type": "scene",
                "text": "Pabellón B",
                "sceneId": "scene_6",
                "id": "scene-6"
            }
        ]
    },
    "scene_14": {
        "title": "Pabellón A-3",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/14.jpg',
        "hfov": default_fov,
        "pitch": -1,
        "yaw": 29,
        "hotSpots": [                
            {
                "pitch": -1,
                "yaw": -91,
                "type": "scene",
                "text": "Pabellón A-4",
                "sceneId": "scene_16",
                "id": "scene-16"
            },
            {
                "pitch": -1,
                "yaw": -34,
                "type": "scene",
                "text": "Muestra Afro",
                "sceneId": "scene_9",
                "id": "scene-9"
            },
            {
                "pitch": -4,
                "yaw": 3,
                "type": "scene",
                "text": "Danza en la ciudad",
                "sceneId": "scene_15",
                "id": "scene-15"
            },
            {
                "pitch": -3,
                "yaw": 61,
                "type": "scene",
                "text": "Opre Rromá",
                "sceneId": "scene_3",
                "id": "scene-3"
            },
            {
                "pitch": -2,
                "yaw": 113,
                "type": "scene",
                "text": "Circuitos artísticos étnicos",
                "sceneId": "scene_18",
                "id": "scene-18"
            },
            {
                "pitch": -1,
                "yaw": 154,
                "type": "scene",
                "text": "Pabellón A-2",
                "sceneId": "scene_17",
                "id": "scene-17"
            }
        ]
    },
    "scene_15": {
        "title": "Danza en la ciudad",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/15.jpg',
        "hfov": default_fov,
        "pitch": 2,
        "yaw": -2,
        "hotSpots": [                
            {
                "pitch": 0,
                "yaw": 31,
                "type": "scene",
                "text": "Hytcha Guy",
                "sceneId": "scene_1",
                "id": "scene-1"
            },
            {
                "pitch": -1,
                "yaw": -91,
                "type": "scene",
                "text": "Pabellón A-3",
                "sceneId": "scene_14",
                "id": "scene-14"
            },
            {
                "pitch": 2,
                "yaw": -2,
                "type": "info",
                "text": "Danza en la ciudad",
                "cssClass": "popup-click",
                "id": "popup12",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-danza-en-la-ciudad"
            }
        ]
    },
    "scene_16": {
        "title": "Pabellón A-4",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/16.jpg',
        "hfov": default_fov,
        "pitch": -3,
        "yaw": -5,
        "hotSpots": [                
            {
                "pitch": -2,
                "yaw": -140,
                "type": "scene",
                "text": "Espacio General",
                "sceneId": "scene_5",
                "id": "scene-5"
            },
            {
                "pitch": 0,
                "yaw": -82,
                "type": "scene",
                "text": "Arte para la interculturalidad",
                "sceneId": "scene_4",
                "id": "scene-4"
            },
            {
                "pitch": -2,
                "yaw": 25,
                "type": "scene",
                "text": "Muestra Afro",
                "sceneId": "scene_9",
                "id": "scene-9"
            },
            {
                "pitch": 0,
                "yaw": 77,
                "type": "scene",
                "text": "Danza en la ciudad",
                "sceneId": "scene_15",
                "id": "scene-15"
            },
            {
                "pitch": 0,
                "yaw": -45,
                "type": "scene",
                "text": "Por los senderos del arte indígena",
                "sceneId": "scene_7",
                "id": "scene-7"
            },
            {
                "pitch": -6,
                "yaw": 122,
                "type": "scene",
                "text": "Pabellón A-3",
                "sceneId": "scene_14",
                "id": "scene-14"
            }
        ]
    },
    "scene_17": {
        "title": "Pabellón A-2",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/17.jpg',
        "hfov": default_fov,
        "pitch": 0,
        "yaw": 3,
        "hotSpots": [                
            {
                "pitch": -4,
                "yaw": -7,
                "type": "scene",
                "text": "Hytcha Guy",
                "sceneId": "scene_1",
                "id": "scene-1"
            },
            {
                "pitch": -2,
                "yaw": -129,
                "type": "scene",
                "text": "Pabellón A-3",
                "sceneId": "scene_14",
                "id": "scene-14"
            },
            {
                "pitch": -1,
                "yaw": -71,
                "type": "scene",
                "text": "Circuitos artísticos étnicos",
                "sceneId": "scene_18",
                "id": "scene-18"
            },
            {
                "pitch": -1,
                "yaw": 60,
                "type": "scene",
                "text": "Exposición Embera",
                "sceneId": "scene_19",
                "id": "scene-19"
            },
            {
                "pitch": -1,
                "yaw": -100,
                "type": "scene",
                "text": "Opre Rroma",
                "sceneId": "scene_3",
                "id": "scene-3"
            },
            {
                "pitch": -1,
                "yaw": 91,
                "type": "scene",
                "text": "Becas Idartes",
                "sceneId": "scene_2",
                "id": "scene-2"
            },
            {
                "pitch": -6,
                "yaw": 115,
                "type": "scene",
                "text": "Pabellón A-1",
                "sceneId": "scene_10",
                "id": "scene-10"
            }
        ]
    },
    "scene_18": {
        "title": "Circuitos artísticos étnicos",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/18.jpg',
        "hfov": default_fov,
        "pitch": 21,
        "yaw": 174,
        "hotSpots": [                
            {
                "pitch": 5,
                "yaw": -10,
                "type": "scene",
                "text": "Pabellón A-2",
                "sceneId": "scene_17",
                "id": "scene-17"
            },
            {
                "pitch": -1,
                "yaw": 175,
                "type": "info",
                "text": "Circuitos artísticos",
                "cssClass": "popup-click",
                "id": "popup13",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-circuitos-artisticos"
            }
        ]
    },
    "scene_19": {
        "title": "Sala Embera",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/19.jpg',
        "hfov": default_fov,
        "pitch": 5,
        "yaw": -7,
        "hotSpots": [  
            {
                "pitch": -5,
                "yaw": 171,
                "type": "scene",
                "text": "Pabellón A-2",
                "sceneId": "scene_17",
                "id": "scene-17"
            },              
            {
                "pitch": -15,
                "yaw": -63,
                "type": "info",
                "text": "Danza Membure",
                "cssClass": "popup-click",
                "id": "popup14",
                "type": "info",
                "URL": "https://www.youtube.com/watch?v=1woV5_3Pz0Q&ab_channel=NewronaSAS"
            }/*Añadir cuando este listo,
            {
                "pitch": -15,
                "yaw": 55,
                "type": "info",
                //Cambiar
                "text": "Danza Membure",
                "cssClass": "popup-click",
                "id": "popup15",
                "type": "info",
                "URL": "https://www.youtube.com/watch?v=1woV5_3Pz0Q&ab_channel=NewronaSAS"
            }*/
        ]
    },
    "scene_20": {
        "title": "Mesas de Arte",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/20.jpg',
        "hfov": default_fov,
        "pitch": 0,
        "yaw": -4,
        "hotSpots": [                
            {
                "pitch": 4,
                "yaw": -175,
                "type": "scene",
                "text": "Pabellón A-1",
                "sceneId": "scene_10",
                "id": "scene-10"
            },
            {
                "pitch": -15,
                "yaw": -2,
                "type": "info",
                "text": "Mesas de Arte",
                "cssClass": "popup-click",
                "id": "popup16",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-mesas-del-arte"
            }
        ]
    },
    "scene_21": {
        "title": "Manuel Zapata Olivella",
        "type": "equirectangular",
        "panorama": templateUrl+'/assets/images/'+device+'/21.jpg',
        "hfov": default_fov,
        "pitch": -1,
        "yaw": -5,
        "hotSpots": [ 
            {
                "pitch": -3,
                "yaw": -130,
                "type": "scene",
                "text": "Pabellón A-1",
                "sceneId": "scene_10",
                "id": "scene-10"
            },            
            {
                "pitch": -17,
                "yaw": -46,
                "type": "info",
                "text": "Fabulas de Tamalameque",
                "cssClass": "popup-click",
                "id": "popup17",
                "type": "info",
                "URL": "https://nencatacoa.co/biblioteca/publicaciones/fabulas-de-tamalameque-2/"
            },
            {
                "pitch": -20,
                "yaw": -6,
                "type": "info",
                "text": "La Calle 10",
                "cssClass": "popup-click",
                "id": "popup18",
                "type": "info",
                "URL": "https://nencatacoa.co/biblioteca/publicaciones/la-calle-10-2/"
            }
        ]
    }
}

var cookies=false;
var searchParam=false;

if(urlParams.has('scene')){
    default_scene = urlParams.get('scene');
	searchParam=true;
}
else{
    checkCookie();
}

var tour=pannellum.viewer('panorama', {   
    "default": {
        "firstScene": default_scene,
        "sceneFadeDuration": 1000,
        "autoLoad": false
    },

    "scenes": sceneList
});

if(searchParam || cookies){
	showPanorama();
}

$("#full-button" ).click(function() {
    $("#masthead").toggle(); 
    $(".footer-width-fixer").toggle(); 
    $(".music-absolute").toggleClass("adjust");
});  

tour.on('scenechange', function (){
    $('#load-menu').css('opacity', '0');
    $('#load-menu').show();    
    $('#load-menu').animate({opacity: 1}, 500);    
});

tour.on('load', function (){
    $('#load-menu').hide();
    setCookie("scene", tour.getScene(), 30);
    gtag('event', 'scene_viewed', {
        'scene': tour.getScene()
    });
});

$("#hide-button" ).click(function() {
    $( "#hide-menu" ).slideUp("slow");   
    showPanorama(); 
    playMusic();
});   

$('.button-int').click(function() {
    var id=$(this).attr('id');
    if($(this).hasClass(id+"-hidden")){
        hideAll();
    }
    $(this).toggleClass(id+"-hidden");
    $("#"+id+"-toggle").toggleClass('flip');    
});

function hideAll(){
    $(".button-int").each(function(index, item){
        var el=$(item).attr('id');
        $(item).addClass(el+"-hidden");
        $("#"+el+"-toggle").removeClass("flip"); 
    });
}

function showPanorama(){
    tour.loadScene(default_scene);
    $(" #panorama ").show();
    $( "#hide-menu" ).hide();
    $( "#panorama-controls" ).show();
    $( "#masthead" ).show();
    document.querySelector("#masthead").style.visibility = "visible"; 
    $(".boton-home").find('a:first').attr('href', "https://nencatacoa.co/?scene=scene_5");
}

$("#button-open-menu").click(function() {
    openMenu();
});  

$("#button-close-menu").click(function() {
    hideMenu();
});  

$("#overlay-menu").click(function() {
    hideMenu();
});

function openMenu(){
    $("#overlay-menu").show();
}

function hideMenu(){
    $("#overlay-menu").hide();
}

var switchplay = true;
var audioElement = document.getElementById('audioplayer');

audioElement.volume=0.2;

var promise = audioElement.play();

if (promise !== undefined) {
  promise.then(_ => {
    changeMusic();
  }).catch(error => {
    // Autoplay was prevented.
  });
}

function changeMusic(){
    if(switchplay){
        audioElement.play();
        $("#icon1").hide();
        $("#icon2").show();
    }
    else{
        audioElement.pause();  
        $("#icon2").hide();
        $("#icon1").show();          
    }
    switchplay=!switchplay; 
}

function playMusic(){
    audioElement.play();
    $("#icon1").hide();
    $("#icon2").show();
    switchplay=false;
}

function setCookie(cname,cvalue,exminutes) {
  var d = new Date();
  d.setTime(d.getTime() + (exminutes*60*1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie() {
  var actualScene=getCookie("scene");
  if (actualScene != "") {
        var keys = [];
        for (var k in sceneList) keys.push(k);       
        if(keys.includes(actualScene)){
            default_scene=actualScene;
			cookies=true;
        }                
  } else {
    setCookie("scene", default_scene, 30);
  }
}

function mouseupListener(event) {
    console.log("Mouse Up - "+tour.getScene());
    var posPitch = tour.mouseEventToCoords(event)[0];
    var posYaw = tour.mouseEventToCoords(event)[1];
    console.log('"pitch": '+Math.round(posPitch)+',\n"yaw": '+Math.round(posYaw)+',');
}

function activateTracking(){
    tour.on('mouseup', mouseupListener);
}

</script>

<?php get_footer(); ?>