var default_fov=120;

document.onreadystatechange = function() { 
	if (document.readyState !== "complete") { 
		document.querySelector("body").style.visibility = "hidden"; 
        document.querySelector("#hide-menu").style.visibility = "visible"; 
        $('#menu').animate({opacity: 0}, 200);
        $('#menu').animate({opacity: 1}, 500);
		document.querySelector("#hide-menu").style.visibility = "visible"; 
        $('#inside-menu').animate({opacity: 0}, 500);
        $('#inside-menu').animate({opacity: 1}, 500);
	} else { 
		document.querySelector("body").style.visibility = "visible"; 
	} 
}; 

var openPopUp = function(args, popup) {
    $("#overlay").hide();
    $("#popup-frame").attr('src', popup);
}

var closePopUp = function() {
    $("#overlay").show();
}


var sceneList={
    "scene_1": {
        "title": "Hytcha Guy",
        "type": "equirectangular",
        "panorama": "images/1.jpg",
        "hfov": default_fov,
        "pitch": 7,
        "yaw": -179,
        "hotSpots": [                
            {
                "pitch": 4,
                "yaw": 2,
                "type": "scene",
                "text": "Pabellón A",
                "sceneId": "scene_17",
                "id": "scene-17"
            },
            {
                "pitch": 7,
                "yaw": -179,
                "type": "info",
                "text": "Pop Up",
                "cssClass": "popup-click",
                "id": "popup1",
                "clickHandlerFunc": openPopUp,
                //Cambiar
                "clickHandlerArgs" : "/home-experience/popup1"
            }
        ]
    },
    "scene_2": {
        "title": "Becas General",
        "type": "equirectangular",
        "panorama": "images/2.jpg",
        "pitch": -4,
        "yaw": 176,
        "hfov": default_fov,
        "hotSpots": [                
            {
                "pitch": 6,
                "yaw": 1,
                "type": "scene",
                "text": "Pabellón A",
                //Cambiar (?)
                "sceneId": "scene_10",
                "id": "scene-10"
            },
            {
                "pitch": -4,
                "yaw": 176,
                "type": "scene",
                "text": "Becas",
                "sceneId": "scene_8",
                "id": "scene-8"
            }
        ]
    },
    "scene_3": {
        "title": "Opré Romá",
        "type": "equirectangular",
        "panorama": "images/3.jpg",
        "pitch": 2,
        "yaw": 177,
        "hfov": default_fov,
        "hotSpots": [                
            {
                "pitch": 10,
                "yaw": -7,
                "type": "scene",
                "text": "Pabellón A",
                //Cambiar (?)
                "sceneId": "scene_14",
                "id": "scene-14"
            },
            {
                "pitch": 2,
                "yaw": 177,
                "type": "info",
                "text": "Opré Romá",
                "cssClass": "popup-click",
                "id": "popup2",
                "clickHandlerFunc": openPopUp,
                //Cambiar
                "clickHandlerArgs" : "/home-experience/popup1"
            }
        ]
    },
    "scene_4": {
        "title": "Arte para la interculturalidad",
        "type": "equirectangular",
        "panorama": "images/4.jpg",
        "hfov": default_fov,
        "pitch": 8,
        "yaw": -1,
        "hotSpots": [                
            {
                "pitch": 2,
                "yaw": -177,
                "type": "scene",
                "text": "Pabellón A",
                //Cambiar (?)
                "sceneId": "scene_16",
                "id": "scene-16"
            },
            {
                "pitch": 8,
                "yaw": -1,
                "type": "info",
                "text": "Artes interculturales",
                "cssClass": "popup-click",
                "id": "popup3",
                "clickHandlerFunc": openPopUp,
                //Cambiar
                "clickHandlerArgs" : "/home-experience/popup1"
            }
        ]
    },
    "scene_5": {
        "title": "Espacio General",
        "type": "equirectangular",
        "panorama": "images/5.jpg",
        "hfov": default_fov,
        "pitch": -2,
        "yaw": 6,
        "hotSpots": [                
            {
                "pitch": -2,
                "yaw": -151,
                "type": "scene",
                "text": "Pabellón B",
                "sceneId": "scene_6",
                "id": "scene-6"
            },
            {
                "pitch": -4,
                "yaw": 56,
                "type": "scene",
                "text": "Pabellón A",
                // (?)
                "sceneId": "scene_16",
                "id": "scene-16"
            },
            {
                "pitch": -20,
                "yaw": 80,
                "type": "info",
                "text": "Pop Up",
                "cssClass": "popup-click",
                "id": "popup4",
                "clickHandlerFunc": openPopUp,
                //Cambiar
                "clickHandlerArgs" : "popup1window"
            },
            {
                "pitch": -20,
                "yaw": 100,
                "type": "info",
                "text": "Pop Up",
                "cssClass": "popup-click",
                "id": "popup5",
                "type": "info",
                //Cambiar
                "URL": "https://artbma.org/"
            }
        ]
    },
    "scene_6": {
        "title": "Pabellón B",
        "type": "equirectangular",
        "panorama": "images/6.jpg",
        "hfov": default_fov,
        "pitch": 1,
        "yaw": 8,
        "hotSpots": [                
            {
                "pitch": -2,
                "yaw": -6,
                "type": "scene",
                "text": "Espacio General",                    
                "sceneId": "scene_5",
                "id": "scene-5"
            },
            {
                "pitch": -3,
                "yaw": -121,
                "type": "scene",
                "text": "Fechas Conmemorativas",                    
                "sceneId": "scene_11",
                "id": "scene-11"
            },
            {
                "pitch": -1,
                "yaw": 66,
                "type": "scene",
                "text": "Eventos no Idartes con representatividad de impacto en la ciudad",                    
                "sceneId": "scene_13",
                "id": "scene-13"
            },
            {
                "pitch": -1,
                "yaw": 123,
                "type": "scene",
                "text": "Calendario artístico de los grupos étnicos",                    
                "sceneId": "scene_12",
                "id": "scene-12"
            }
        ]
    },
    "scene_7": {
        "title": "Por los senderos del arte indígena en Bogotá",
        "type": "equirectangular",
        "panorama": "images/7.jpg",
        "hfov": default_fov,
        "pitch": 9,
        "yaw": -180,
        "hotSpots": [                
            {
                "pitch": 6,
                "yaw": -2,
                "type": "scene",
                "text": "Pabellón A",
                //Cambiar (?)
                "sceneId": "scene_16",
                "id": "scene-16"
            },
            {
                "pitch": 9,
                "yaw": -180,
                "type": "info",
                "text": "Por los senderos del arte indígena en Bogotá",
                "cssClass": "popup-click",
                "id": "popup6",
                "clickHandlerFunc": openPopUp,
                //Cambiar
                "clickHandlerArgs" : "/home-experience/popup1"
            }
        ]
    },
    "scene_8": {
        "title": "Becas",
        "type": "equirectangular",
        "panorama": "images/8.jpg",
        "hfov": default_fov,
        "pitch": 4,
        "yaw": -7,
        "hotSpots": [                
            {
                "pitch": 6,
                "yaw": 178,
                "type": "scene",
                "text": "Pabellón A",
                //Cambiar (?)
                "sceneId": "scene_10",
                "id": "scene-10"
            },
            {
                "pitch": -5,
                "yaw": -67,
                "type": "info",
                "text": "TBD",
                "cssClass": "popup-click",
                "id": "popup7",
                "clickHandlerFunc": openPopUp,
                //Cambiar
                "clickHandlerArgs" : "/home-experience/popup1"
            },
            {
                "pitch": -4,
                "yaw": -43,
                "type": "info",
                "text": "TBD",
                "cssClass": "popup-click",
                "id": "popup8",
                "clickHandlerFunc": openPopUp,
                //Cambiar
                "clickHandlerArgs" : "/home-experience/popup1"
            },
            {
                "pitch": -4,
                "yaw": -6,
                "type": "info",
                "text": "TBD",
                "cssClass": "popup-click",
                "id": "popup9",
                "clickHandlerFunc": openPopUp,
                //Cambiar
                "clickHandlerArgs" : "/home-experience/popup1"
            },
            {
                "pitch": -6,
                "yaw": 32,
                "type": "info",
                "text": "TBD",
                "cssClass": "popup-click",
                "id": "popup10",
                "clickHandlerFunc": openPopUp,
                //Cambiar
                "clickHandlerArgs" : "/home-experience/popup1"
            }
        ]
    },
    "scene_9": {
        "title": "Muestra Afro",
        "type": "equirectangular",
        "panorama": "images/9.jpg",
        "hfov": default_fov,
        "pitch": 11,
        "yaw": -174,
        "hotSpots": [                
            {
                "pitch": 6,
                "yaw": -5,
                "type": "scene",
                "text": "Pabellón A",
                //Cambiar (?)
                "sceneId": "scene_14",
                "id": "scene-14"
            }
        ]
    },
    "scene_10": {
        "title": "Pabellón A-1",
        "type": "equirectangular",
        "panorama": "images/10.jpg",
        "hfov": default_fov,
        "hotSpots": [                
            {
                "pitch": -1,
                "yaw": -161,
                "type": "scene",
                "text": "Manuel Zapata Olivella",
                //Cambiar
                "sceneId": "scene_11",
                "id": "scene-11"
            },
            {
                "pitch": -2,
                "yaw": -108,
                "type": "scene",
                "text": "Espacio General",
                "sceneId": "scene_5",
                "id": "scene-5"
            },
            {
                "pitch": 0,
                "yaw": 34,
                "type": "scene",
                "text": "Pabellón A-2",
                "sceneId": "scene_14",
                "id": "scene-14"
            },
            {
                "pitch": -1,
                "yaw": 100,
                "type": "scene",
                "text": "Becas Idartes",
                //Cambiar (?)
                "sceneId": "scene_2",
                "id": "scene-2"
            },
            {
                "pitch": -4,
                "yaw": 139,
                "type": "scene",
                "text": "Mesas de arte",
                //Cambiar
                "sceneId": "scene_11",
                "id": "scene-11"
            }
        ]
    },
    "scene_11": {
        "title": "Sala de fechas conmemorativas",
        "type": "equirectangular",
        "panorama": "images/11.png",
        "hfov": default_fov,
        "pitch": 7,
        "yaw": -43,
        "hotSpots": [                
            {
                "pitch": 1,
                "yaw": 162,
                "type": "scene",
                "text": "Pabellón B",
                "sceneId": "scene_6",
                "id": "scene-6"
            }
        ]
    },
    "scene_12": {
        "title": "Calendario artístico de los grupos étnicos",
        "type": "equirectangular",
        "panorama": "images/12.jpg",
        "hfov": default_fov,
        "pitch": 4,
        "yaw": -5,
        "hotSpots": [                
            {
                "pitch": -2,
                "yaw": -176,
                "type": "scene",
                "text": "Pabellón B",
                "sceneId": "scene_6",
                "id": "scene-6"
            },
            {
                "pitch": 4,
                "yaw": -5,
                "type": "info",
                "text": "Calendario artístico de los grupos étnicos",
                "cssClass": "popup-click",
                "id": "popup11",
                "clickHandlerFunc": openPopUp,
                //Cambiar
                "clickHandlerArgs" : "/home-experience/popup1"
            }
        ]
    },
    "scene_13": {
        "title": "Eventos no Idartes con representatividad de impacto en la ciudad",
        "type": "equirectangular",
        "panorama": "images/13.png",
        "hfov": default_fov,
        "pitch": 3,
        "yaw": 34,
        "hotSpots": [                
            {
                "pitch": -4,
                "yaw": -174,
                "type": "scene",
                "text": "Pabellón B",
                "sceneId": "scene_6",
                "id": "scene-6"
            }
        ]
    },
    "scene_14": {
        "title": "Pabellón A-3",
        "type": "equirectangular",
        "panorama": "images/14.png",
        "hfov": default_fov,
        "pitch": -1,
        "yaw": 29,
        "hotSpots": [                
            {
                "pitch": -1,
                "yaw": -91,
                "type": "scene",
                "text": "Pabellón A-4",
                "sceneId": "scene_1",
                "id": "scene-1"
            },
            {
                "pitch": -1,
                "yaw": -34,
                "type": "scene",
                "text": "Muestra Afro",
                "sceneId": "scene_9",
                "id": "scene-9"
            },
            {
                "pitch": -4,
                "yaw": 3,
                "type": "scene",
                "text": "Danza en la ciudad",
                "sceneId": "scene_15",
                "id": "scene-15"
            },
            {
                "pitch": -2,
                "yaw": 65,
                "type": "scene",
                "text": "Opré Romá",
                "sceneId": "scene_3",
                "id": "scene-3"
            },
            {
                "pitch": -1,
                "yaw": 154,
                "type": "scene",
                "text": "Pabellón A-2",
                "sceneId": "scene_14",
                "id": "scene-14"
            }
        ]
    },
    "scene_15": {
        "title": "Danza en la ciudad",
        "type": "equirectangular",
        "panorama": "images/15.jpg",
        "hfov": default_fov,
        "pitch": 2,
        "yaw": -2,
        "hotSpots": [                
            {
                "pitch": 0,
                "yaw": 31,
                "type": "scene",
                "text": "Hytcha Guy",
                "sceneId": "scene_1",
                "id": "scene-1"
            },
            {
                "pitch": -1,
                "yaw": -91,
                "type": "scene",
                "text": "Pabellón A-3",
                "sceneId": "scene_14",
                "id": "scene-14"
            },
            {
                "pitch": 2,
                "yaw": -2,
                "type": "info",
                "text": "Danza en la ciudad",
                "cssClass": "popup-click",
                "id": "popup12",
                "clickHandlerFunc": openPopUp,
                //Cambiar
                "clickHandlerArgs" : "/home-experience/popup1"
            }
        ]
    },
    "scene_16": {
        "title": "Pabellón A-4",
        "type": "equirectangular",
        "panorama": "images/16.jpg",
        "hfov": default_fov,
        "pitch": -3,
        "yaw": -5,
        "hotSpots": [                
            {
                "pitch": -2,
                "yaw": -140,
                "type": "scene",
                "text": "Espacio General",
                "sceneId": "scene_5",
                "id": "scene-5"
            },
            {
                "pitch": -2,
                "yaw": -50,
                "type": "scene",
                "text": "Arte para la interculturalidad",
                "sceneId": "scene_4",
                "id": "scene-4"
            },
            {
                "pitch": -4,
                "yaw": 27,
                "type": "scene",
                "text": "Por los senderos del arte indígena",
                "sceneId": "scene_7",
                "id": "scene-7"
            },
            {
                "pitch": -6,
                "yaw": 122,
                "type": "scene",
                "text": "Pabellón A-3",
                "sceneId": "scene_17",
                "id": "scene-17"
            }
        ]
    },
    "scene_17": {
        "title": "Pabellón A-2",
        "type": "equirectangular",
        "panorama": "images/17.png",
        "hfov": default_fov,
        "pitch": 0,
        "yaw": 3,
        "hotSpots": [                
            {
                "pitch": 0,
                "yaw": 3,
                "type": "scene",
                "text": "Hytcha Guy",
                "sceneId": "scene_1",
                "id": "scene-1"
            },
            {
                "pitch": -1,
                "yaw": -118,
                "type": "scene",
                "text": "Pabellón A-3",
                "sceneId": "scene_17",
                "id": "scene-17"
            },
            {
                "pitch": -2,
                "yaw": -59,
                "type": "scene",
                "text": "Circuitos artísticos étnicos",
                //Cambiar
                "sceneId": "scene_1",
                "id": "scene-1"
            },
            {
                "pitch": -1,
                "yaw": 67,
                "type": "scene",
                "text": "Eposición embera",
                //Cambiar
                "sceneId": "scene_1",
                "id": "scene-1"
            },
            {
                "pitch": -6,
                "yaw": 124,
                "type": "scene",
                "text": "Pabellón A-1",
                "sceneId": "scene_10",
                "id": "scene-10"
            }
        ]
    },
    "scene_18": {
        "title": "Circuitos artísticos étnicos",
        "type": "equirectangular",
        "panorama": "images/18.jpg",
        "hfov": default_fov,
        "pitch": 0,
        "yaw": 0,
        "hotSpots": [                
            {
                "pitch": 0,
                "yaw": 0,
                "type": "scene",
                "text": "Pabellón A-2",
                "sceneId": "scene_17",
                "id": "scene-17"
            },
            {
                "pitch": 2,
                "yaw": 166,
                "type": "info",
                "text": "Circuitos artísticos",
                "cssClass": "popup-click",
                "id": "popup13",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-circuitos-artisticos"
            }
        ]
    },
    "scene_19": {
        "title": "Sala Embera",
        "type": "equirectangular",
        "panorama": "images/19.jpg",
        "hfov": default_fov,
        "pitch": 7,
        "yaw": 0,
        "hotSpots": [  
            {
                "pitch": -5,
                "yaw": 178,
                "type": "scene",
                "text": "Pabellón A-2",
                "sceneId": "scene_17",
                "id": "scene-17"
            },              
            {
                "pitch": -12,
                "yaw": -41,
                "type": "info",
                "text": "Danza Membure",
                "cssClass": "popup-click",
                "id": "popup14",
                "type": "info",
                "URL": "https://www.youtube.com/watch?v=1woV5_3Pz0Q&ab_channel=NewronaSAS"
            },
            {
                "pitch": -14,
                "yaw": 47,
                "type": "info",
                //Cambiar
                "text": "Danza Membure",
                "cssClass": "popup-click",
                "id": "popup15",
                "type": "info",
                "URL": "https://www.youtube.com/watch?v=1woV5_3Pz0Q&ab_channel=NewronaSAS"
            }
        ]
    },
    "scene_20": {
        "title": "Mesas de Arte",
        "type": "equirectangular",
        "panorama": "images/20.jpg",
        "hfov": default_fov,
        "pitch": 0,
        "yaw": -4,
        "hotSpots": [                
            {
                "pitch": 1,
                "yaw": -171,
                "type": "scene",
                "text": "Pabellón A-1",
                "sceneId": "scene_10",
                "id": "scene-10"
            },
            {
                "pitch": -12,
                "yaw": -4,
                "type": "info",
                "text": "Mesas de Arte",
                "cssClass": "popup-click",
                "id": "popup16",
                "clickHandlerFunc": openPopUp,
                "clickHandlerArgs" : "/home-experience/popup-mesas-del-arte"
            }
        ]
    },
    "scene_21": {
        "title": "Manuel Zapata Olivella",
        "type": "equirectangular",
        "panorama": "images/21.jpg",
        "hfov": default_fov,
        "pitch": 0,
        "yaw": 6,
        "hotSpots": [ 
            {
                "pitch": -3,
                "yaw": -130,
                "type": "scene",
                "text": "Pabellón A-1",
                "sceneId": "scene_10",
                "id": "scene-10"
            },            
            {
                "pitch": -1,
                "yaw": -34,
                "type": "info",
                "text": "Fabulas de Tamalameque",
                "cssClass": "popup-click",
                "id": "popup17",
                "type": "info",
                "URL": "https://nencatacoa.co/biblioteca/publicaciones/fabulas-de-tamalameque-2/"
            },
            {
                "pitch": -1,
                "yaw": 6,
                "type": "info",
                "text": "La Calle 10",
                "cssClass": "popup-click",
                "id": "popup18",
                "type": "info",
                "URL": "https://nencatacoa.co/biblioteca/publicaciones/la-calle-10-2/"
            }
        ]
    }
}

var tour=pannellum.viewer('panorama', {   
    "default": {
        "firstScene": "scene_12",
        "sceneFadeDuration": 1000,
        "autoLoad": true
    },
    "scenes": sceneList
});

function mouseupListener(event) {
    console.log("Mouse Up - "+tour.getScene());
    var posPitch = tour.mouseEventToCoords(event)[0];
    var posYaw = tour.mouseEventToCoords(event)[1];
    console.log('"pitch": '+Math.round(posPitch)+',\n"yaw": '+Math.round(posYaw)+',');
};

tour.on('mouseup', mouseupListener);

$("#hide-button" ).click(function() {
    $( "#hide-menu" ).slideUp("slow");
});   

$('.button-int').click(function() {
    var id=$(this).attr('id');
    if($(this).hasClass(id+"-hidden")){
        hideAll();
    }
    $(this).toggleClass(id+"-hidden");
    $("#"+id+"-toggle").toggleClass('flip');    
});

function hideAll(){
    $(".button-int").each(function(index, item){
        var el=$(item).attr('id');
        $(item).addClass(el+"-hidden");
        $("#"+el+"-toggle").removeClass("flip"); 
    });
}

var switchplay = true;
var audioElement = document.getElementById('audioplayer');
function changeMusic(){
    if(switchplay){
        audioElement.play();
        $("#icon1").hide();
        $("#icon2").show();
    }
    else{
        audioElement.pause();  
        $("#icon2").hide();
        $("#icon1").show();          
    }
    switchplay=!switchplay; 
}